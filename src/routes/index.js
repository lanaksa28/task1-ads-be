const express = require("express");
const router = express.Router();
const authenticationMiddleware = require("../middleware/auth");
const uploadMiddleware = require("../middleware/multer");
const isTokenValidMiddleware = require("../middleware/checkToken");

const {
  register,
  login,
  sendOtp,
  editUserProfile,
} = require("../controllers/main");

router.route("/register").post(register);
router.route("/login").post(login);
router.route("/sendOtp").post(sendOtp);
router
  .route("/user-profile")
  .patch(
    authenticationMiddleware,
    isTokenValidMiddleware,
    uploadMiddleware.single("photo-profile"),
    editUserProfile
  );

module.exports = router;

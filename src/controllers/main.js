const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const { BadRequestError, CustomAPIError } = require("../errors");
const prisma = require("../db/prisma");
const generateRandomCode = require("../util/generateRandomeCode");

const register = async (req, res) => {
  const data = req.body;
  const { fullName, email, username, phoneNumber } = data;

  if (!fullName || !email || !username || !phoneNumber)
    throw new BadRequestError("Complete the form");

  if ((await prisma.user.findUnique({ where: { email: email } })) !== null)
    throw new BadRequestError("used email");
  if (
    (await prisma.user.findUnique({ where: { username: username } })) !== null
  )
    throw new BadRequestError("used username");
  if (
    (await prisma.user.findUnique({ where: { phoneNumber: phoneNumber } })) !==
    null
  )
    throw new BadRequestError("used phone number");

  const registeredUser = await prisma.user.create({
    data: { fullName, email, username, phoneNumber },
  });

  res.status(201).json(registeredUser);
};

const sendOtp = async (req, res) => {
  const { phoneNumber } = req.body;

  if (!phoneNumber) throw new BadRequestError("Insert Phone Number");

  const user = await prisma.user.findUnique({
    where: { phoneNumber: phoneNumber },
  });

  if (user === null)
    throw new CustomAPIError("Phone Number is not registered", 404);

  const otpModel = prisma.otp;
  const isThereOtp = await otpModel.findUnique({
    where: { phoneNumber: phoneNumber },
  });

  if (isThereOtp)
    await otpModel.delete({ where: { phoneNumber: phoneNumber } });

  // generate OTP
  const otpCode = generateRandomCode();

  //send original OTP (otpCode) to user

  //send hashed OTP to db
  const saltRounds = 7;
  const hashedOtp = await bcrypt.hash(otpCode.toString(), saltRounds);

  await prisma.otp.create({
    data: {
      hashedOtp: hashedOtp,
      phoneNumber: phoneNumber,
    },
  });

  res.status(201).json({ msg: "OTP is sent", otp: otpCode });
};

const login = async (req, res) => {
  const { otpCode, phoneNumber } = req.body;

  if (!otpCode || !phoneNumber)
    throw new BadRequestError("insert phone number and otp code");

  if (otpCode.length !== 6) throw new BadRequestError("Otp code must be 6");

  const user = await prisma.user.findUnique({
    where: { phoneNumber: phoneNumber },
  });

  if (user === null)
    throw new CustomAPIError("Phone Number is not registered", 404);

  const userOtp = await prisma.otp.findUnique({
    where: { phoneNumber: phoneNumber },
  });

  if (!userOtp) throw new CustomAPIError("Could not found otp code", 404);
  const { hashedOtp } = userOtp;

  if (await bcrypt.compare(otpCode.toString(), hashedOtp)) {
    const { id, fullName, email } = user;
    const token = jwt.sign(
      {
        id,
        fullName,
        email,
        phoneNumber,
      },
      process.env.JWT_SECRET,
      { expiresIn: "24h" }
    );

    const deleteOtp = await prisma.otp.delete({
      where: { phoneNumber: phoneNumber },
    });
    console.log(deleteOtp);
    const isThereToken = await prisma.token.findUnique({
      where: { userId: id },
    });

    if (isThereToken) await prisma.token.delete({ where: { userId: id } });

    const saveToken = await prisma.token.create({
      data: {
        token: token,
        userId: id,
      },
    });

    console.log(saveToken);

    res.status(201).json({
      msg: "Login success",
      data: {
        id,
        fullName,
        email,
        phoneNumber,
      },
      token,
    });
  } else {
    throw new BadRequestError("Otp Code is Wrong");
  }
};

const editUserProfile = async (req, res) => {
  const fullName = req.body.fullName;
  const photoProfile = req.file;
  const userId = req.user.id;

  if (!userId) throw new BadRequestError("relogin please");

  const editedUser = await prisma.user.update({
    where: { id: userId },
    data: { fullName: fullName },
  });

  if (photoProfile) {
    const isTherePhoto = await prisma.userProfile.findUnique({
      where: { userId: userId },
    });

    if (isTherePhoto)
      await prisma.userProfile.delete({ where: { userId: userId } });

    const photoProfilePath = photoProfile.path;
    const addPhotoProfile = await prisma.userProfile.create({
      data: { photoProfile: photoProfilePath, userId: userId },
    });
    res
      .status(201)
      .json({ msg: "data updated", data: { editedUser, addPhotoProfile } });
  } else {
    res.status(201).json({ msg: "data updated", data: { editedUser } });
  }
};

module.exports = { register, login, sendOtp, editUserProfile };

require("dotenv").config();
require("express-async-errors");

const express = require("express");
const app = express();
const port = process.env.PORT || 9000;
const notFound = require("./middleware/not-found");
const errorHandlerMiddleware = require("./middleware/error-handler");

const swaggerUi = require("swagger-ui-express");
const apiDoc = require("../docs/apiDocJson.json");

const routes = require("./routes");

app.use(express.json());
app.use("/api/v1/task1", routes);
app.use("/api/v1/documentation", swaggerUi.serve, swaggerUi.setup(apiDoc));
app.use(notFound);
app.use(errorHandlerMiddleware);

const start = async () => {
  try {
    app.listen(port, () => {
      console.log(`server is listening on http://localhost:${port}`);
    });
  } catch (error) {
    console.error(error);
  }
};

start();

const jwt = require("jsonwebtoken");
const { UnauthenticatedError } = require("../errors");

const authenticationMiddleware = async (req, res, next) => {
  const authHeader = req.headers.authorization;

  if (!authHeader || !authHeader.startsWith("Bearer ")) {
    throw new UnauthenticatedError("no token provided");
  }

  const token = authHeader.split(" ")[1];
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    const { id, fullName, email, phoneNumber } = decoded;
    req.user = { id, fullName, email, phoneNumber };
    next();
  } catch (error) {
    throw new UnauthenticatedError("No access");
  }
};

module.exports = authenticationMiddleware;

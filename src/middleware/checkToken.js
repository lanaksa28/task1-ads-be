const { UnauthenticatedError } = require("../errors");
const prisma = require("../db/prisma");

const isTokenValidMiddleware = async (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader.split(" ")[1];
  const { id } = req.user;
  const isThereToken = await prisma.token.findUnique({
    where: { token: token, userId: id },
  });

  if (!isThereToken) {
    throw new UnauthenticatedError("Token has been expired");
  } else {
    next();
  }
};

module.exports = isTokenValidMiddleware;

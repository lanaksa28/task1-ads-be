/*
  Warnings:

  - A unique constraint covering the columns `[hashedOtp]` on the table `Otp` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[phoneNumber]` on the table `Otp` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `Otp_hashedOtp_key` ON `Otp`(`hashedOtp`);

-- CreateIndex
CREATE UNIQUE INDEX `Otp_phoneNumber_key` ON `Otp`(`phoneNumber`);
